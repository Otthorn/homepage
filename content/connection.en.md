---
title: Connection to the network
menu:
  main:
    weight: 3
toc: true
---

The WiFi network at the Crans is protected by `WPA2-Enterprise` authentication
and the wired network is protected by an authentication by captive portal.
In both cases the connection requires a login and a password.

No manipulation on the intranet is required to add
a new machine: at the first connection
**your device will be automatically registered on your intranet account**.

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Username and password</p>
  </div>
  <div class="message-body is-size-5">
    The username and password needed to connect to the network
    are the same as for connecting to
    the <a href="https://intranet.crans.org/">intranet</a>.
  </div>
</article>

<article class="message is-warning">
  <div class="message-header is-size-5">
    <p>Do not log in with an account other than yours!</p>
  </div>
  <div class="message-body is-size-5">
    The machine MUST NOT be already registered on another account!
    If you lend your identifiers, then this other person will
    no longer be able to register his machine on the network.
    If this is the case you need to contact us.
  </div>
</article>

<div class="divider"></div>

## WiFi on Windows 8, 8.1, et 10

Select the WiFi network `Cr@ns`, and enter your login and password directly.
You should have internet access in less than 10 minutes.

If you have a security alert regarding our certificates,
ignore it the first time you connect and validate the certificate.

If you wish to have slightly more speed but less distance,
you can log on to `Cr@ns-5g`.

<div class="divider"></div>

## WiFi on Windows XP, Vista ou 7

<article class="message is-warning">
  <div class="message-header is-size-5">
    <p>Windows 7 support has expired!</p>
  </div>
  <div class="message-body is-size-5">
    Microsoft has decided to no longer distribute security updates
    for Windows 7 since January 14, 2020.
    We strongly encourage you to upgrade to Windows 10 or Linux
    which in both cases is free.
  </div>
</article>

We no longer support theses operating systems because
they are no longer secure and cause security problems for the network.

<div class="divider"></div>

## WiFi on MacOS

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Instability on 2.4 GHz network</p>
  </div>
  <div class="message-body is-size-5">
    On updated Apple machines, a bug in the implementation of 2.4 GHz WiFi
    makes the connection unstable on the `Cr@ns` network. To counteract this, you
    can connect in 5 GHz to the `Cr@ns-5g` network.
  </div>
</article>

Select the WiFi network `Cr@ns-5g` or `Cr@ns`
and enter your login and password directly.
You should have internet access in less than 10 minutes.

{{< figure src="/images/content/wifi-macos.png" title="" >}}

<div class="divider"></div>

## WiFi on Android

Select the WiFi network `Cr@ns-5g` or `Cr@ns`.

Please choose:

  * EAP method: `PEAP`,
  * Authentication step 2: `MSCHAPV2`,
  * Enter your login and password directly.

You should have internet access in less than 10 minutes.

{{< figure src="/images/content/wifi-android.jpg" title="" >}}

<div class="divider"></div>

## WiFi on iOS

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Instability on 2.4 GHz network</p>
  </div>
  <div class="message-body is-size-5">
    On updated Apple machines, a bug in the implementation of 2.4 GHz WiFi
    makes the connection unstable on the `Cr@ns` network. To counteract this, you
    can connect in 5 GHz to the `Cr@ns-5g` network.
  </div>
</article>

Select the WiFi network `Cr@ns-5g` or `Cr@ns`
and enter your login and password directly.
You should have internet access in less than 10 minutes.

At the first connection the device asks you to rely on our certificate.

{{< figure src="/images/content/wifi-ios.jpg" title="" >}}

<div class="divider"></div>

## WiFi on Linux

The vast majority of Linux distributions (Ubuntu, Debian, Fedora...)
use the NetworkManager tool to manage the WiFi connection.

Select the wifi network `Cr@ns` or `Cr@ns-5g` then :

  * Authentication: `Secure EAP (PEAP)`,
  * Phase 2 authentication: `MSCHAPV2`,
  * Check the box `No CA certificate required`, unless you want to download
    every 3 months our certificate,
  * Enter your login and password directly.

You should have internet access in less than 10 minutes.

{{< figure src="/images/content/wifi-linux.jpg" title="" >}}

<div class="divider"></div>

## WiFi on other devices

For game consoles, the vast majority (Xbox, Playstation, Nintendo
Switch...) do not support `WPA2-Enterprise'.
If you wish to connect this device, then you must either go through
the Ethernet cable, or buy a WiFi router that you plug into the Ethernet port
of your room to have your own WiFi network.

The advantage of a router is to create your own private network, isolating your
other users' home devices.

<div class="divider"></div>

## Wired network

Each room has an "RJ45" Ethernet port.
If we do not have a cable to connect your computer,
one free cable will be given to you, when you first joined.
You can also purchase a USB/USB-C to Ethernet adapter at any time.

Connect directly by cable, in your room for example.
A pop-up window should open. If it doesn't, go to
<http://intranet.crans.org/>.
Then identify yourself with your username and password
and follow the instructions.

You should have internet access in less than 10 minutes.
You should also receive an email notification. 

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>No web browser?</p>
  </div>
  <div class="message-body is-size-5">
    If you have a machine that can't surf the internet
    (e.g. a WiFi router), then you will need to manually create your
    wired machine.
    Get the MAC address of your machine, then log in to
    <a href="https://intranet.crans.org">the intranet</a>.
    Then click on create a machine, fill in the MAC and let the
    other parameters unchanged. Finally, connect the device to your outlet.
  </div>
</article>
