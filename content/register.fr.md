---
title: Inscription
menu:
  main:
    weight: 2
toc: true
---

## Étape 1 : Connexion ou création d'un compte

Pour avoir internet il faut avoir un compte,
être adhérent à jour de l'association
et avoir payé une connexion internet.

<article class="message is-warning">
  <div class="message-header is-size-5">
    <p>Attention ! Ne créez pas de nouveau compte si vous en avez déjà un.</p>
  </div>
  <div class="message-body is-size-5">
    Ne créer pas un second compte.
    Votre connexion ne fonctionnera alors pas correctement.
    Si vous avez déjà été adhérent au Crans, vous disposez forcément
    d'un compte. Si vous avez oublié votre mot de passe,
    cliquez <a href="https://intranet.crans.org/users/reset_password/">ici</a>.
    Si vous avez également oublié votre identifiant, contactez-nous par mail. 
  </div>
</article>

**Si vous avez déjà un compte Crans**,
connectez-vous à [l'intranet](https://intranet.crans.org/)
avec vos identifiants.

**Si vous êtes nouveau sur le campus et n'avez pas de compte**, rendez-vous
sur [la page de création de compte sur l'intranet](https://intranet.crans.org/users/new_user/).
Remplissez alors les informations demandées puis validez.
Le Crans est une association Loi 1901, vous devez en accepter les statuts,
le réglement intérieur et leurs annexes.
La validation de ce formulaire et le réglement de la cotisation
vaut pour adhésion à l'association.

## Étape 2 : Payer son adhésion et sa connexion

Vous pouvez payer en ligne par carte bancaire.
Si vous souhaitez payer par liquide, chèque ou autre moyen, rendez-vous
à une permanence à la Kfet.

### Payer en carte bancaire

**Si vous souhaitez une connexion pour un an**, vous pouvez payer directement 50€ et
obtenir une adhésion et une connexion internet pendant un an.
**Si vous ne souhaitez pas une connexion pour un an**,
alors il faut adhérer (à l'année) pour 10€ puis payer 5€ par mois de connexion.

Si vous payez au mois, vous pouvez charger votre solde du bon montant puis
l'utiliser pour payer l'adhésion et les mois. Cela évite de devoir effectuer
plusieurs transactions bancaires.

Sur [la page de votre profil](https://intranet.crans.org/users/mon_profil/) cliquez sur « *Payez une connexion* ».
Ensuite sélectionnez « *Comnpay (Carte En Ligne)* » puis sélectionnez les articles.

Notez qu'il faut être adhérent de l'association pour que l'on puisse vous
vendre une connexion internet.

## Étape 3 : Vérification

Si tout s'est bien passé, vous devriez voir sur [votre profil](https://intranet.crans.org/users/mon_profil/) un
indicateur « *Connexion* » vert indiquant que vous pouvez maintenant
[connecter votre machine au réseau]({{< ref "connection" >}}).

Vous pouvez également maintenant profiter des différents services qu'offre le Crans à ses adhérents.
La liste des services est disponible sur [la page d'accueil de l'intranet](https://intranet.crans.org/).
