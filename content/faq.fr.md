---
title: Foire aux questions
menu:
  main:
    weight: 5
toc: true
---

## J'ai perdu mon accès à mon compte Crans :(

> Tu peux réinitialiser ton mot de passe en cliquant [ici](https://intranet.crans.org/users/reset_password/).

## Je veux recharger mon solde Crans

> Tu peux le faire soit en permanence à la Kfet, soit sur l'intranet par CB, [ici](https://intranet.crans.org/users/mon_profil/), onglet "recharger". 

##  Lorsque je souhaite prolonger ma connexion au mois, on me demande de payer davantage, alors que la connexion à l'année est annoncée à 50€.

> L'adhésion et la connexion pour une année sont en effet proposées à 50€.
> Nous proposons cependant un forfait au mois, pour 10€ de frais d'adhésion puis 5€ par mois de connexion.
> Si tu restes toute l'année, payer au mois revient donc plus cher, nous t'encourageons ainsi à payer à l'année pour limiter notre charge de travail. 

## Je veux gérer mes machines WiFi ou filaire.

> Directement sur l'intranet, [ici](https://intranet.crans.org/users/mon_profil/).

## Quels services offrez-vous aux adhérents ?

> Les adhérents peuvent accèder à l'ensemble des services listés sur [la page d'accueil de l'intranet](https://intranet.crans.org/).

## Quels services offrez-vous aux anciens adhérents ?

> Les anciens adhérents peuvent encore utiliser l'ensemble des services hormis le service d'accès à internet.

## Mon internet a l'air lent.

> Tu peux commencer par vérifier qu'il n'y a pas de problème global, en regardant notamment sur notre [Facebook](https://www.facebook.com/CachanReseauNormaleSup/) ou notre [Twitter @TwCrans](https://twitter.com/twcrans) si on y a décrit un incident en cours.
>
> Si tu es aventureux, tu peux également aller consulter [notre service de monitoring](https://grafana.crans.org).

## Je travaille tous les jours de 12h à 20h et ne peux donc pas être disponible pendant une permanence. Suis-je condamné à m'acheter une livebox ?

> Si tu es déjà adhérent, il te suffit de prolonger ta connexion par internet.
> Sinon, nous t'invitons à envoyer un email à cableurs@crans.org en précisant tes disponibilités afin qu'un membre du Crans puisse trouver du temps pour s'occuper de toi.

## Y a-t-il une Box Crans ? Comment brancher plusieurs ordinateurs ?

> Nous ne proposons pas de box Internet.
> Chaque chambre Crous est déjà équipée d'une prise RJ45 sur laquelle tu peux brancher ton ordinateur, et le WiFi est diffusé par des points d'accès répartis dans les bâtiments.
>
> Pour brancher plusieurs ordinateurs à la fois, tu peux te procurer un commutateur ("switch") réseau, ou un routeur WiFi avec plusieurs prises réseau.

## Comment brancher ma console de jeu ?

> Notre réseau WiFi requiert un protocole d'authentification spécial, appelé "WPA2 Entreprise", qui n'est pas toujours supporté par les consoles de jeux.
> En revanche, se connecter par câble ethernet ne requiert aucun protocole particulier.
> Une solution consiste donc à se procurer un adaptateur ethernet adapté à ta console.
> Une solution alternative consiste à créer ton propre réseau WiFi à l'aide d'un ordinateur branché par câble partageant la connexion par WiFi ou d'un routeur WiFi.

## J'ai vu des membres du Crans à la KFet à 23h mais ils n'ont pas voulu régler mon souci d'internet.

> Les membres de notre association sont bénévoles, et consacrent déjà beaucoup de temps pour l'association, il est important de respecter leur vie privée.

## Je n'arrive pas à me connecter à photos.crans.org, note.crans.org...

> Ces services ne sont pas gérés directement par le Crans mais par des adhérents,
> en cas de soucis contacter respectivement les administrateurs de ces sites.
