---
title: Connexion au réseau
menu:
  main:
    weight: 3
toc: true
---

Le WiFi au Crans est protégé par une authentification `WPA2-Entreprise`
et la connexion filaire est protégée par une authentification par
portail captif.
Dans les deux cas la connexion requiert un identifiant et un mot de passe.

Aucune manipulation sur l'intranet n'est nécessaire pour ajouter
une nouvelle machine : à la première connexion,
**votre appareil sera enregistré automatiquement sur votre compte intranet**.

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Identifiant et mot de passe</p>
  </div>
  <div class="message-body is-size-5">
    L'identifiant et le mot de passe nécessaires à la connexion au réseau
    sont les mêmes que pour vous connecter à
    l'<a href="https://intranet.crans.org/">intranet</a>.
  </div>
</article>

<article class="message is-warning">
  <div class="message-header is-size-5">
    <p>Ne vous connectez pas avec un autre compte que le vôtre !</p>
  </div>
  <div class="message-body is-size-5">
    La machine NE DOIT PAS être déjà enregistrée sur un autre compte !
    Si vous prêtez vos identifiants, alors cette autre personne ne pourra
    plus inscrire sa machine sur le réseau.
    Si c'est le cas vous devez nous contacter.
  </div>
</article>

<div class="divider"></div>

## WiFi sur Windows 8, 8.1, et 10

Sélectionnez le réseau WiFi `Cr@ns`, et entrez directement vos identifiants.
Vous devriez bénéficier d'un accès internet en moins de 10 minutes.

Si vous avez une alerte de sécurité concernant nos certificats,
ignorez la à la première connexion et validez le certificat.

Si vous souhaitez avoir légèrement plus de vitesse mais moins de distance,
vous pouvez vous connecter sur `Cr@ns-5g`.

<div class="divider"></div>

## WiFi sur Windows XP, Vista ou 7

<article class="message is-warning">
  <div class="message-header is-size-5">
    <p>Le support de Windows 7 a expiré !</p>
  </div>
  <div class="message-body is-size-5">
    Microsoft a décidé de ne plus distribuer des mises à jour de sécurité
    pour Windows 7 depuis le 14 janvier 2020.
    Nous vous encourageons très fortement à passer sur Windows 10 ou Linux
    ce qui est dans les deux cas gratuit.
  </div>
</article>

Nous ne supportons plus ce système d'exploitation car il n'est plus
sécurisé et engendre des problèmes de sécurité pour le réseau.
Si vous venez en permanence nous pouvons vous aider à mettre à niveau
vers Windows 10 ou Linux à condition que vous ayez fait une sauvegarde de vos
données et que vous signiez une décharge.

<div class="divider"></div>

## WiFi sur MacOS

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Instabilité en 2,4 GHz</p>
  </div>
  <div class="message-body is-size-5">
    Sur les machines Apple à jour, un bug dans l'implémentation du WiFi 2,4 GHz
    rend la connexion instable sur le réseau `Cr@ns`. Pour contrer cela, vous
    pouvez vous connecter en 5 GHz sur le réseau `Cr@ns-5g`.
  </div>
</article>

Sélectionnez le réseau WiFi `Cr@ns-5g` ou `Cr@ns`,
et entrez directement vos identifiants.
Vous devriez bénéficier d'un accès internet en moins de 10 minutes.

{{< figure src="/images/content/wifi-macos.png" title="" >}}

<div class="divider"></div>

## WiFi sur Android

Sélectionnez le réseau WiFi `Cr@ns-5g` ou `Cr@ns`.

Choisissez :

  * Méthode EAP : `PEAP`,
  * Authentification étape 2 : `MSCHAPV2`,
  * Entrez directement votre identifiants.

Vous devriez bénéficier d'un accès internet en moins de 10 minutes.

{{< figure src="/images/content/wifi-android.jpg" title="" >}}

<div class="divider"></div>

## WiFi sur iOS

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Instabilité en 2,4 GHz</p>
  </div>
  <div class="message-body is-size-5">
    Sur les machines Apple à jour, un bug dans l'implémentation du WiFi 2,4 GHz
    rend la connexion instable sur le réseau `Cr@ns`. Pour contrer cela, vous
    pouvez vous connecter en 5 GHz sur le réseau `Cr@ns-5g`.
  </div>
</article>

Sélectionnez le réseau WiFi `Cr@ns-5g` ou `Cr@ns`,
et entrez directement vos identifiants.
Vous devriez bénéficier d'un accès internet en moins de 10 minutes.

À la première connexion l'appareil vous demande de vous fier à notre certificat.

{{< figure src="/images/content/wifi-ios.jpg" title="" >}}

<div class="divider"></div>

## WiFi sur une distribution Linux

La grande majorité des distributions Linux (Ubuntu, Debian, Fedora...)
utilise l'outil NetworkManager pour gérer la connexion WiFi.

Sélectionnez le réseau wifi `Cr@ns` ou `Cr@ns-5g` puis :

  * Authentification : `EAP Sécurisé (PEAP)`,
  * Authentification de phase 2 : `MSCHAPV2`,
  * Cochez la case `Aucun certificat CA requis`, sauf si vous voulez télécharger
    tous les 3 mois notre certificat,
  * Entrez directement vos identifiants.

Vous devriez bénéficier d'un accès internet en moins de 10 minutes.

{{< figure src="/images/content/wifi-linux.jpg" title="" >}}

<div class="divider"></div>

## WiFi sur les autres périphériques

Pour les consoles de jeux, la grande majorité (Xbox, Playstation, Nintendo
Switch...) ne supportent pas le `WPA2-Entreprise`.
Si vous souhaitez connecter ce périphérique, alors il faut soit passer par
le câble Ethernet, soit acheter un routeur WiFi que vous branchez dans
votre chambre pour avoir votre propre réseau WiFi.

L'avantage d'un routeur est de créer votre propre réseau privé, isolant votre
périphérique domestique des autres usagers.

<div class="divider"></div>

## Câble

Chaque chambre dispose d'une prise Ethernet « RJ45 ».
Si nous n'avez pas de câble pour connecter votre ordi,
un câble vous sera remis **gratuitement** en permanence,
lors de votre première adhésion.
Vous pouvez également acheter en permanence un adaptateur USB/USB-C
vers Ethernet.

Connectez-vous directement par câble, dans votre chambre par exemple.
Un pop-up devrait s'ouvrir. Si ce n'est pas le cas, allez sur
<http://intranet.crans.org/>.
Identifiez-vous alors avec votre identifiant et mot de passe
et suivez les instructions.

Vous devriez bénéficier d'un accès internet en moins de 10 minutes.
Vous devriez également recevoir une notification par courriel. 

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Pas de navigateur Web ?</p>
  </div>
  <div class="message-body is-size-5">
    Si vous avez une machine qui ne peut pas naviguer sur internet
    (par exemple un routeur WiFi), alors vous devrez créer manuellement votre
    machine filaire.
    Munissez-vous de l'adresse MAC de votre machine, puis connectez-vous à
    <a href="https://intranet.crans.org">l'intranet</a>.
    Cliquez alors sur créer une machine, renseignez la MAC et laissez les
    autres paramètres inchangés. Enfin, connectez l'appareil à votre prise.
  </div>
</article>
