---
title: Qui sommes-nous ?
menu:
  main:
    weight: 1
---

**Le Crans s'est fixé pour but d'assurer aux résidents de la
résidence du CROUS de Cachan une connection Internet filaire et WiFi.**
Chaque logement est aujourd'hui câblé, et la couverture WiFi est presque
complète.

Bien que son principal objectif reste la gestion du réseau informatique du
campus afin d'assurer à ses **1300 adhérents** un accès Internet,
le Crans a développé de nombreuses autres activités.
Aujourd'hui l'association fournit également à ses adhérents des services tels
qu'[une boîte mail à vie](https://intranet.crans.org/users/mon_profil/#email),
[un hébergement de pages personnelles](https://perso.crans.org/),
[la télévision](https://tv.crans.org/)...

**Le Crans a également un but pédagogique** ;
nous organisons par exemple
[des séminaires techniques hebdomadaires](https://wiki.crans.org/CransTechnique/CransApprentis/SeminairesTechniques)
sur des thèmes généraux ou spécifiques au Crans,
comme la gestion d'un réseau et les protocoles et services associés.

L'association a aussi pour but de promouvoir en toute occasion l'utilisation
des logiciels libres auprès de ses adhérents comme auprès du grand public :
organisation d'événements comme [les install party](https://install-party.crans.org/)
annuelles, qui sont également l'occasion de conférences,
mais aussi par exemple hébergement d'un miroir de [VideoLAN](https://www.videolan.org/), de [Debian](https://www.debian.org/) et d'[Ubuntu](https://ubuntu.com/).
L'association est également adhérente de l'[APRIL](https://april.org/).
Le Crans est membre de [FedeRez](https://www.federez.net/),
fédération d'associations étudiantes visant à
coordonner les échanges entre associations d'informatique.

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>Je veux aider !</p>
  </div>
  <div class="message-body is-size-5">
    Si tu as envie de découvrir l'envers du décor, que ce soit la gestion
    administrative d'une association à grande échelle ou la gestion technique
    de l'infrastructure, tu es le/la bienvenu(e). Il n'y a pas de connaissance
    particulière à avoir pour commencer, mais si tu nous rejoins, tu auras
    l'occasion d'en acquérir de nombreuses. Pour commencer, le plus simple est
    de prendre contact avec nous, par exemple en nous croisant à la Kfet pendant
    une permanence. À bientôt !
  </div>
</article>
