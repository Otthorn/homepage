---
title: Registration
menu:
  main:
    weight: 2
toc: true
---

## Step 1: Login or create an account

To have internet you must have an account,
be a current member of the association
and pay for an internet connection.

<article class="message is-warning">
  <div class="message-header is-size-5">
    <p>Look out! Do not create a new account if you already have one.</p>
  </div>
  <div class="message-body is-size-5">
    Do not create a second account.
    Your connection will then not work properly.
    If you have already been a member of the Crans, you must already have
    an account. If you forgot your password,
    go to <a href="https://intranet.crans.org/users/reset_password/">this page</a>.
    If you have also forgotten your username, please contact us by email.
  </div>
</article>

**If you already have a Crans account**,
connect to [the intranet](https://intranet.crans.org/)
with your credentials.

**If you're new on campus and don't have an account**, go to
[the account creation page on the intranet](https://intranet.crans.org/users/new_user/).
Fill in the requested information and validate.
The Crans is a non-profit organization, you must accept its status,
the rules of procedure and their annexes.
The validation of this form and the payment of the contribution
is valid for membership of the association.

## Step 2: Pay for your membership and connection

You can pay online by credit card.
If you wish to pay by cash, cheque or other means, please go to
to a permanence at the Kfet.

### Pay by credit card

**If you wish to join for one year**, you can pay directly 50€ and
get a membership and an internet connection for one year.
**If you do not wish to join for one year**,
then you have to subscribe for 10€ and then pay 5€ per month of connection.

If you pay by the month, you can charge your balance in the correct amount and then
use it to pay the membership and the months. This avoids having to
several bank transactions.

On [your profile page](https://intranet.crans.org/users/mon_profil/) click on "*Pay for a connection*".
Then select "*Comnpay (Online Card)*" and select the items.

Please note that you must be a member of the association in order for us to be able to
sell you an internet connection.

## Step 3: Verification


If all went well, you should see on [your profile](https://intranet.crans.org/users/mon_profil/) a
green "*Connection*" indicator indicating that you can now
[connect your machine to the network]({{< ref "connection" >}}).

You can now also take advantage of the various services that Crans offers to its members.
The list of services is available on [the intranet homepage](https://intranet.crans.org/).
