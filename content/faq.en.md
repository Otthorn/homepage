---
title: Frequently Asked Questions
menu:
  main:
    weight: 5
toc: true
---


## I've lost access to my Crans account :(

> You can reset your password by clicking [here](https://intranet.crans.org/users/reset_password/).

## I want to refill my Crans balance

> You can do it either at the Kfet, or on the intranet by credit card, [here](https://intranet.crans.org/users/mon_profil/). 

## When I try to extend my connection to the month, I am asked to pay more whereas the connection to the year is announced at 50€.

> Membership and connection for one year are indeed proposed at 50€.
> However, we offer a monthly plan, for €10 membership fee and then €5 per month of connection.
> If you stay the whole year, paying by the month is more expensive, so we encourage you to pay by the year to limit our workload. 

## I want to manage my WiFi or wired machines.

> Directly on the intranet, [here](https://intranet.crans.org/users/mon_profil/).

## What services do you offer to members?

> Members can access all the services listed on [the home page of the intranet](https://intranet.crans.org/).

## What services do you offer to former members?

> Former members can still use all services except the Internet access.

## My internet seems slow.

> You can start by checking that there is no global problem by looking on our [Facebook](https://www.facebook.com/CachanReseauNormaleSup/) or [Twitter @TwCrans](https://twitter.com/twcrans).
>
> If you are adventurous, you can also go and check out [our monitoring service](https://grafana.crans.org).

## I work every day from 12:00 to 8:00 pm and therefore can't be available to pay my connection. Am I doomed to buy a livebox?

> If you are already a member, all you have to do is extend your Internet connection.
> Otherwise, we invite you to send an email to cableurs@crans.org specifying your availability so that a member of the Crans can find time to take care of you.

## Is there a Crans Box? How do you connect multiple computers?

> We don't offer Internet Boxes.
> Each CROUS room is already equipped with an RJ45 jack where you can plug in your computer, and WiFi is distributed through access points throughout the buildings.
>
> To connect several computers at the same time, you can get a network switch, or a WiFi router with multiple network interfaces.

## How do I connect my game console?

> Our WiFi network requires a special authentication protocol called "WPA2 Enterprise", which isn't always supported by game consoles.
> On the other hand, connecting via ethernet cable does not require any special protocol.
> One solution is to get an ethernet adapter adapted to your console.
> An alternative solution is to create your own WiFi network using a computer sharing a WiFi connection or a WiFi router.

## I saw some members of the KFet at 11pm but they wouldn't fix my internet problem.

> The members of our association are volunteers and are already devoting a lot of time to the association, it's important to respect their privacy.

## I can't connect to photos.crans.org, note.crans.org...

> These services are not managed directly by the Crans but by members.
> In case of problems, please contact the administrators of these sites.
