---
title: About us
menu:
  main:
    weight: 1
---

**The Crans has the goal of ensuring that residents
of the CROUS de Cachan get a wired Internet connection and WiFi.**
Each accommodation is now wired, and WiFi coverage is almost
complete.

Although its main objective remains the management of the
campus in order to ensure Internet access for its 1300 members,
the Crans has developed many other activities.
Today the association also provides its members with services such as
a [lifetime mailbox](https://intranet.crans.org/users/mon_profil/#email),
[a hosting of personal pages](https://perso.crans.org/),
[television](https://tv.crans.org/)...

**The Crans also has an educational purpose** ;
we organize for example
[weekly technical seminars](https://wiki.crans.org/CransTechnique/CransApprentis/SeminairesTechniques)
on general or specific Crans themes,
such as network management and associated protocols and services.

The association also aims to promote at all times the use of the
free software to its members and the general public:
organizing events like [the install party](https://install-party.crans.org/)
annual meetings, which are also the occasion for conferences,
but also for example hosting a mirror of [VideoLAN](https://www.videolan.org/), [Debian](https://www.debian.org/) and [Ubuntu](https://ubuntu.com/).
The association is also a member of [APRIL](https://april.org/).
The Crans is a member of [FedeRez](https://www.federez.net/),
student union federation aimed at
coordinate exchanges between computer associations.

<article class="message is-info">
  <div class="message-header is-size-5">
    <p>I want to help!</p>
  </div>
  <div class="message-body is-size-5">
    If you would like to get to know the other side of the coin, be it the administrative management of a large-scale association or the technical management of the infrastructure, you are very welcome. There is no particular knowledge to begin with, but if you join us, you will have the opportunity to acquire a lot of it. The easiest way to start is to get in touch with us, for example by meeting us at the Kfet during a shift. See you soon!
  </div>
</article>
