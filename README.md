# Documentation de démarrage

Site statique minimaliste pour aiguiller un nouvel utilisateur.

Il utilise le générateur de site static Hugo.

## Tester le site en local

 1. Installer `hugo`, par exemple avec APT sous Debian/Ubuntu.

 2. Ensuite dans le dépôt vous pouvez simplement appeler `hugo server`.

NB : si vous éditez le style, l'option `--disableFastRender` peut être utile.

## Générer

Lancez `hugo` puis copiez le contenu généré du dossier `public/` vers un serveur web.

Pour changer l'URL de base, vous pouvez utiliser l'option `-b`.

## Quels sont les outils utilisés ?

  * [CSS Bulma](https://bulma.io/) : `themes/crans/assets/bulma`,
  * [Thème Bulma](https://github.com/jenil/bulmaswatch/blob/gh-pages/darkly/) : `themes/crans/assets/bulmatheme`,
  * Font Awesome généré avec Fontello.
